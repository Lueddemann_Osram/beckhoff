Device Type anlegen
Device anlegen mit dem zugehörigen device type
Key generieren mit
openssl ecparam -out device_key.pem -name prime256v1 -genkey
4. Key in private key umwandeln mittel
Anlegen einer neuen Datei namens private_key.pem
Den Output von openssl pkcs8 -topk8 -nocrypt -in device_key.pem in die private_key.pem Datei schreiben
Device Certificate erzeugen
openssl req -new -key private_key.pem -x509 -days 365 -out device_cert.pem -subj ‘/O=My-Tenant/CN=My-Device’
Achtung my tenant und my device mit den dazugehörigen IDs ersetzen
Device Cert in die OLT registries für das device
Kopiere den text aus 
sed 's/$/\\n/' device_cert.pem | tr -d '\n'
Text aus dem sed Befehl mittels API auf das dazugehörige device registrieren/pushen
https://api.preview.oltd.de/v1/devices/{{deviceId}}/certificates
Erstelle ein File namens olt_ca.pem mit dem inhalt 

-----BEGIN CERTIFICATE-----
MIICBzCCAaygAwIBAgIBADAKBggqhkjOPQQDAjBcMQswCQYDVQQGEwJERTEOMAwG
A1UEChMFT1NSQU0xDDAKBgNVBAsTA09MVDEvMC0GA1UEAxMmT1NSQU0gT0xUIERl
dmljZVNlcnZpY2VzIFRydXN0QW5jaG9yIDEwIBcNMTgwNjEyMTU1NTMwWhgPMjA1
ODA2MTIxNTU1MzBaMFwxCzAJBgNVBAYTAkRFMQ4wDAYDVQQKEwVPU1JBTTEMMAoG
A1UECxMDT0xUMS8wLQYDVQQDEyZPU1JBTSBPTFQgRGV2aWNlU2VydmljZXMgVHJ1
c3RBbmNob3IgMTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABIRHefdjW8eKPEpi
RV88sqk/7nqOIDdg4v2KcIsX8LQD94YGkDEDO4Alg3EdtibTXMtztbSiRMmy/BeB
7Fmbr+KjXTBbMB0GA1UdDgQWBBQmEJ8uur+FfHaFxDYw1oeYNu1M6TAfBgNVHSME
GDAWgBQmEJ8uur+FfHaFxDYw1oeYNu1M6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQE
AwIBBjAKBggqhkjOPQQDAgNJADBGAiEA1dAeBWcIDUyOzuQSzhO0cajg3mZfiHp/
NwryIKRR9fgCIQDKqKmKv1STjPEePu4NL2YEqsVauaVl4CVQIYVjEwN3cw==
-----END CERTIFICATE-----



MQTT.fx Einstellungen vornehmen
ACHTUNG: Bei Client Key File das private_key.pem file benutzen!
Verbinden
Daten Senden
